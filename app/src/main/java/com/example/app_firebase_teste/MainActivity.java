package com.example.app_firebase_teste;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase db;
    DatabaseReference ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = FirebaseDatabase.getInstance();
        ref = db.getReference();

        //ref.child("usuarios").child("id").child("01").child("nome").setValue("Josias");


    /*    Usuario user = new Usuario("02","Josias", 53, "josias@email.com");
        ref.child("usuarios").push().setValue(user);

        user = new Usuario("02","Leôncio", 25, "bolinea.de.gorfe@email.com");
        ref.child("usuarios").push().setValue(user);

        user = new Usuario("02","Marina", 27, "marina@email.com");
        ref.child("usuarios").push().setValue(user);

        user = new Usuario("02","Mauro", 23, "mauro@email.com");
        ref.child("usuarios").push().setValue(user);

        user = new Usuario("02","Jessica", 19, "jessikinhaahhdoorkut@email.com");
        ref.child("usuarios").push().setValue(user);

        user = new Usuario("02","Robin Wood", 33, "flechaflecha@email.com");
        ref.child("usuarios").push().setValue(user);*/

        /*ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i("FIREBASE:",dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
        DatabaseReference refUser = ref.child("usuarios");
        Query usrINQ = refUser.orderByChild("idade").startAt(18).endAt(23);

        usrINQ.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Usuario usr = dataSnapshot.getChildren().iterator().next().getValue(Usuario.class);
                //Log.i("FIREBASE:", "Nome = " + usr.getNome() + " Email = " + usr.getEmail());
                Log.i("FIREBASE ", dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
